# Aliases
alias mountp="sshfs nyuprince:/scratch/ycc520 ~/Desktop/scratch"
alias mountgw="sshfs gwprince:/scratch/ycc520 ~/Desktop/scratch"
alias mountlabp="sshfs nyuprince:/scratch/cgsb/desplan ~/Desktop/scratch"
alias mountlabgw="sshfs gwprince:/scratch/cgsb/desplan ~/Desktop/scratch"
alias vi=nvim
alias vim=nvim


# PATH related
export PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
export MANPATH="/usr/local/opt/coreutils/libexec/gnuman:$MANPATH"
eval `dircolors ~/Documents/dircolors-solarized/dircolors.ansi-light`

# Add ~/shell_script to path
export PATH=$PATH:$HOME/shell_script
# Make imagemagick accessible to Emacs
export PATH="/usr/local/opt/imagemagick@6/bin:$PATH"
